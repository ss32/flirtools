# flirtools

Python script to ease the pain of working with a Flir Boson, particularly color mapping the raw output.  C++ code for motion tracking.  Will work with any camera or video feed.

## Setup

Both the Python and C++ tracking code require [OpenCV](https://opencv.org/) to be installed.  You can [build from source](https://docs.opencv.org/4.x/d7/d9f/tutorial_linux_install.html) or install everything (only required if you want to use both tools) with 

```bash
sudo apt install libopencv-dev python3-opencv
```

or install the Python package only via pip

```bash
python3 -m pip install opencv-python
```

### Building C++ Tool

If you want to use the C++ tool check that `pkg-config` can find the libraries it needs with 

```bash
pkg-config --cflags --libs opencv4
```

You should get a lot of output with things like `-I/usr/local/include/opencv4 -L/usr/local/lib`.  Run `./build.sh` and you should be left with a binary called `./tracker`.

## Usage    

### Python Color Map Tool

Default `python3 main.py` will open a window showing the raw feed in every color map.  Optionally pass `--dev` to specify the video device number or `--video` to save the output.

```
usage: main.py [-h] [--video VIDEO] [--dev DEV]

options:
  -h, --help     show this help message and exit
  --video VIDEO  Filename for the output video. If empty, will not record a video.
  --dev DEV      Device number for the camera, default is 0
```

![maps](./maps.jpg)

### C++ Tracker

Requires OpenCV C++ headers to be built/installed, see the setup section above.  

```terminal
$ ./tracker -h
Usage: ./tracker <video_resource> --raw-framerate <FPS> --motion-framerate <FPS> --raw-filename <NAME> --motion-filename <NAME> --mean-frames <NUM_FRAMES>

video_resource:         Path to the video resource, can be video file or device.
--raw-framerate:        Framerate of the raw video output. Default 30.
--motion-framerate:     Framerate of the motion video output. Default 15
--raw-filename:         Name of the raw video output, no file extension. Default raw_output
--motion-filename:      Name of the motion video output, no file extension. Default motion_output
--mean-frames:          Number of frames to use for the mean.

Example: ./tracker /dev/video0 --raw-framerate 60 --motion-framerate 30 --raw-filename field_test --motion-filename field_test_motion --mean-frames 20
```

Outputs both a raw feed with a unix timestamp as well as a stacked video of any motion in the frame alongside the current live frame subtracted from a rolling average of the past `N` frames, default is 30.  The tool looks for bright spots in the subtracted image to detect changes.  

Currently the video output format is `.avi` because the main loop doesn't exit cleanly and AVI encoding will still play back if the file handle doesn't properly close. The result that is when you open the video you won't know how long it is.  This can be addressed by re-encoding to `mp4` with `ffmpeg -i motion.avi -c:v libx264 motion.mp4`. Sure, there are nice ways to handle `SIGINT` but I threw this together and after trying a few ways gave up and stuck with what works.  PRs to fix this are encouraged.

![raccoon](tracker_screenshot.jpg)

### Todo

 - [ ] Cleanly exit tracking loop
 - [ ] Enable dynamic color map switching via user input
 - [ ] Add arg to resize video output


## Focus Tool

Also included is a focus tool for the Flir Boson that can be 3D printed