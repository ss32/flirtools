#include <algorithm>
#include <chrono>
#include <iostream>
#include <vector>

#include "opencv2/opencv.hpp"
#include "signal.h"

#define PBSTR "||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||"
#define PBWIDTH 60

void printProgress(double percentage) {
  int val = (int)(percentage * 100);
  int lpad = (int)(percentage * PBWIDTH);
  int rpad = PBWIDTH - lpad;
  printf("\r%3d%% [%.*s%*s]", val, lpad, PBSTR, rpad, "");
  fflush(stdout);
}

bool cmdOptionExists(char **begin, char **end, const std::string &option) {
  return std::find(begin, end, option) != end;
}

void put_timestamp_on_frame(cv::Mat frame, std::string timestamp) {
  cv::putText(frame, timestamp, cv::Point(10, 30), cv::FONT_HERSHEY_DUPLEX,
              0.65, cv::Scalar(0, 255, 0), 1);
}

std::vector<cv::Point> track_marker(const cv::Mat &img) {
  // Convert the image to grayscale
  cv::Mat gray;
  cv::cvtColor(img, gray, cv::COLOR_BGR2GRAY);

  // Threshold the image
  cv::Mat thresh;
  cv::threshold(gray, thresh, 30, 255, cv::THRESH_BINARY);

  // Expand the thresholded image
  cv::Mat kernel = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(6, 6));
  cv::dilate(thresh, thresh, kernel, cv::Point(-1, -1), 2);

  // Find contours
  std::vector<std::vector<cv::Point>> contours;
  cv::findContours(thresh, contours, cv::RETR_TREE, cv::CHAIN_APPROX_SIMPLE);
  if (contours.empty()) {
    return std::vector<cv::Point>(); // Return an empty vector if no contours
                                     // are found
  }

  // Find the largest contour
  std::vector<cv::Point> centers;
  for (const auto &c : contours) {
    // Get the bounding rectangle
    cv::Rect rect = cv::boundingRect(c);
    // Get the center of the rectangle
    centers.push_back(
        cv::Point(rect.x + rect.width / 2, rect.y + rect.height / 2));
  }

  // Sort centers based on their x-coordinate
  std::sort(centers.begin(), centers.end(),
            [](const cv::Point &a, const cv::Point &b) { return a.x < b.x; });

  // If a corner is between 612,40 and 634, 58, ignore it
  // The Boson's gain control indicator shows up here
  for (int i = 0; i < centers.size(); i++) {
    if (centers[i].x > 612 && centers[i].x < 634 && centers[i].y > 40 &&
        centers[i].y < 58) {
      centers.erase(centers.begin() + i);
    }
  }

  // If two centers are within 10 pixels, remove the second one
  if (centers.size() > 1 && std::abs(centers[0].x - centers[1].x) < 10) {
    centers.erase(centers.begin() + 1);
  }

  // Keep only the first two centers
  if (centers.size() > 3) {
    centers.resize(3);
  }

  return centers;
}

void put_marker_on_image(cv::Mat frame, std::vector<cv::Point> centers) {
  // Draw a rectangle around each center
  int marker_width = 10;
  for (int i = 0; i < centers.size(); i++) {
    cv::rectangle(
        frame,
        cv::Point(centers[i].x - marker_width, centers[i].y - marker_width),
        cv::Point(centers[i].x + marker_width, centers[i].y + marker_width),
        cv::Scalar(0, 255, 0), 2);
  }
}

cv::VideoWriter make_video(int width, int height, std::string output_path,
                           int framerate) {
  cv::VideoWriter video(output_path,
                        cv::VideoWriter::fourcc('M', 'J', 'P', 'G'), framerate,
                        cv::Size(width, height));
  return video;
}

// Iterates over a vector of images and returns the mean image
cv::Mat getMean(const std::vector<cv::Mat> images) {
  if (images.empty())
    return cv::Mat();

  cv::Mat m(images[0].rows, images[0].cols, CV_64FC3);
  m.setTo(cv::Scalar(0, 0, 0));

  cv::Mat temp;
  for (int i = 0; i < images.size(); ++i) {
    images[i].convertTo(temp, CV_64FC3);
    m += temp / images.size();
  }
  m.convertTo(m, CV_8U);
  return m;
}

int main(int argc, char **argv) {

  if (cmdOptionExists(argv, argv + argc, "-h") || argc < 2) {
    std::cout
        << "Usage: ./tracker <video_resource> --raw-framerate <FPS> "
           "--motion-framerate <FPS> --raw-filename <NAME> --motion-filename "
           "<NAME> --mean-frames <NUM_FRAMES>\n\n"
        << "video_resource:         Path to the video resource, can be video "
           "file or device.\n"
        << "--raw-framerate:        Framerate of the raw video output. Default "
           "30.\n"
        << "--motion-framerate:     Framerate of the motion video output. "
           "Default "
           "15\n"
        << "--raw-filename:         Name of the raw video output, no file "
           "extension. "
           "Default raw_output\n"
        << "--motion-filename:      Name of the motion video output, no file "
           "extension. Default motion_output\n"
        << "--mean-frames:          Number of frames to use for the mean.\n\n"
        << "Example: ./tracker /dev/video0 --raw-framerate 60 "
           "--motion-framerate 30 --raw-filename field_test --motion-filename "
           "field_test_motion --mean-frames 20\n\n";
    return 0;
  }

  std::string raw_filename{"raw_output"};
  std::string motion_filename{"motion_output"};
  int raw_framerate{30};
  int motion_framerate{15};
  int num_mean_frames{30};

  // Parse args
  for (int i = 0; i < argc; i++) {
    std::string arg = argv[i];
    if (arg == "--motion-framerate") {
      motion_framerate = std::stoi(argv[i + 1]);
    } else if (arg == "--raw-framerate") {
      raw_framerate = std::stoi(argv[i + 1]);
    } else if (arg == "--raw-filename") {
      raw_filename = argv[i + 1];
    } else if (arg == "--motion-filename") {
      motion_filename = argv[i + 1];
    } else if (arg == "--mean-frames") {
      num_mean_frames = std::stoi(argv[i + 1]);
    }
  }
  motion_filename += ".avi";
  raw_filename += ".avi";

  std::cout << "Opening resource: " << argv[1] << "\n";
  cv::VideoCapture cap(argv[1]);

  if (!cap.isOpened()) {
    std::cerr << "Error opening resource" << argv[1] << "\n";
    return -1;
  }

  cv::Mat frame;
  std::vector<cv::Mat> mean_frames;
  cap >> frame;
  int width = frame.cols;
  int height = frame.rows;
  cv::VideoWriter videoRaw =
      make_video(width, height, raw_filename, raw_framerate);
  cv::VideoWriter videoMotion =
      make_video(width * 2, height, motion_filename, motion_framerate);
  int num_frames = cap.get(cv::CAP_PROP_FRAME_COUNT);
  int idx = 0;
  int frame_idx = 0;

  int ft;
  int in;
  double cm;
  for (int i = 0; i < num_mean_frames; i++) {
    cap >> frame;
    mean_frames.push_back(frame.clone());
  }
  std::cout << "Processing video. CTRL+C to exit.\n";

  while (1) {
    try {
      cap >> frame;

      if (frame.empty() || frame_idx == num_frames) {
        std::cout << "\nDone!\n";
        break;
      }
      frame_idx += 1;
      mean_frames[frame_idx % 30] = frame.clone();
      // Calculate the mean of mean_frames
      cv::Mat mean_frame = getMean(mean_frames);
      mean_frame.convertTo(mean_frame, CV_8U);
      cv::Mat diff = cv::Mat();
      cv::absdiff(frame, mean_frame, diff);
      std::vector<cv::Point> centers = track_marker(diff);
      // Write to motion video if centers is not none
      auto time_nanoseconds =
          std::chrono::duration_cast<std::chrono::nanoseconds>(
              std::chrono::system_clock::now().time_since_epoch())
              .count();
      put_timestamp_on_frame(frame, std::to_string(time_nanoseconds));
      videoRaw.write(frame);
      if (centers.size() > 0) {
        put_marker_on_image(frame, centers);
        // Stack diff and frame next to each other
        cv::Mat stacked_frame =
            cv::Mat::zeros(frame.rows, frame.cols * 2, frame.type());
        frame.copyTo(stacked_frame(cv::Rect(0, 0, frame.cols, frame.rows)));
        diff.copyTo(
            stacked_frame(cv::Rect(frame.cols, 0, frame.cols, frame.rows)));

        videoMotion.write(stacked_frame);
      }
      // If we're processing a video file, show the progress
      if (num_frames > 0) {
        printProgress((double)idx / num_frames);
        idx += 1;
      }

      if (frame_idx % num_mean_frames == 0) {
        frame_idx = 0;
      }
    } catch (...) {
      std::cout << "An exception occurred. Exiting.\n";
      break;
    }
  }

  cap.release();
  cv::destroyAllWindows();
  videoMotion.release();
  videoRaw.release();
  return 0;
}