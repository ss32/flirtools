import cv2
import numpy as np
import argparse
import os
import subprocess
import time
import matplotlib.pylab as plt
import tqdm
colormaps = [
    cv2.COLORMAP_AUTUMN,
    cv2.COLORMAP_PARULA,
    cv2.COLORMAP_BONE,
    cv2.COLORMAP_PINK,
    cv2.COLORMAP_CIVIDIS,
    cv2.COLORMAP_PLASMA,
    cv2.COLORMAP_COOL,
    cv2.COLORMAP_RAINBOW,
    cv2.COLORMAP_DEEPGREEN,
    cv2.COLORMAP_SPRING,
    cv2.COLORMAP_HOT,
    cv2.COLORMAP_SUMMER,
    cv2.COLORMAP_HSV,
    cv2.COLORMAP_TURBO,
    cv2.COLORMAP_INFERNO,
    cv2.COLORMAP_TWILIGHT,
    cv2.COLORMAP_JET,
    cv2.COLORMAP_TWILIGHT_SHIFTED,
    cv2.COLORMAP_MAGMA,
    cv2.COLORMAP_VIRIDIS,
    cv2.COLORMAP_OCEAN,
    cv2.COLORMAP_WINTER,
]
colormapNames = [
    "AUTUMN",
    "PARULA",
    "BONE",
    "PINK",
    "CIVIDIS",
    "PLASMA",
    "COOL",
    "RAINBOW",
    "DEEPGREEN",
    "SPRING",
    "HOT",
    "SUMMER",
    "HSV",
    "TURBO",
    "INFERNO",
    "TWILIGHT",
    "JET",
    "TWILIGHT_SHIFTED",
    "MAGMA",
    "VIRIDIS",
    "OCEAN",
    "WINTER",
]


colormapDict = dict(zip([i for i, _ in enumerate(colormaps)], colormaps))


def system_call(command: str):
    process = subprocess.Popen(command.split(" "), stdout=subprocess.PIPE, shell=True)
    output, error = process.communicate()
    return output, error


# Function that accepts an image and applies all colormaps, then tiles the reults into a single image
def apply_colormaps(image):
    # Create a list to store the results
    results = []

    results.append(image)

    # Apply each colormap to the image
    for i, colormap in enumerate(colormaps):
        results.append(cv2.applyColorMap(image, colormap))
        # Add the name of the current colormap to the frame
        cv2.putText(
            results[-1],
            colormapNames[i],
            (10, 30),
            cv2.FONT_HERSHEY_SIMPLEX,
            1,
            (255, 255, 255),
            2,
            cv2.LINE_AA,
        )
    cv2.putText(
        results[0],
        "ORIGINAL",
        (10, 30),
        cv2.FONT_HERSHEY_SIMPLEX,
        1,
        (255, 255, 255),
        2,
        cv2.LINE_AA,
    )
    # Append 2 empty frames to results to make a 5x5 square
    results += [np.zeros_like(image)] * 2
    # Tile the results into a single image of 5x5 frames
    results = np.array(results)
    results = results.reshape(5, 5, *image.shape)
    results = results.transpose(0, 2, 1, 3, 4)
    results = results.reshape(5 * image.shape[0], 5 * image.shape[1], 3)
    # Resize results to 50%
    results = cv2.resize(results, (0, 0), fx=0.5, fy=0.5)
    return results


def make_video(args: argparse.Namespace, frame: np.array):
    video_codec = cv2.VideoWriter_fourcc(*"mp4v")
    h,w = frame.shape[:2]
    if args.track:
        w = frame.shape[1] * 2
    # For some reason records video 3.37x faster than real time
    if not args.video.endswith(".mp4"):
        args.video += ".mp4"
    video_out = cv2.VideoWriter(
        args.video,
        video_codec,
        60,
        (w, h),
    )
    return video_out



def make_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--video",
        type=str,
        default="",
        help="Filename for the output video.  If empty, will not record a video.",
    )
    parser.add_argument(
        "--dev",
        type=str,
        default="0",
        help="Device number for the camera, default is 0",
    )
    parser.add_argument(
        "--track",
        action="store_true",
        help="Track objects",
    )
    
    return parser.parse_args()


def main():
    args = make_args()
    cv2.namedWindow("Colormaps")
    try:
        dev = int(args.dev)
        isFile = False
        progbar = None
    except:
        dev = args.dev
        isFile = True
    vc = cv2.VideoCapture(dev)
    if isFile:
        nframes= int(vc.get(cv2.CAP_PROP_FRAME_COUNT))
        progbar = tqdm.tqdm(total=nframes)
    print("Press ESC to exit")
    print("--------------------------")
    print("0: ORIGINAL")
    for i, name in enumerate(colormapNames):
        print(f"{i+1}: {name}")
    if vc.isOpened():  # try to get the first frame
        ret, frame = vc.read()
    else:
        print("Error: Couldn't open the video device.")
        exit(1)
    if args.video:
        video_out = make_video(args, frame)
    if ret:
        frame = apply_colormaps(frame)
        cv2.imshow("Colormaps", frame)
        ret, frame = vc.read()
        print("Press ESC to close the window and select a colormap.")
        cv2.waitKey(0)
    cv2.destroyAllWindows()
    ## Initialize values
    selection = int(input("Select a colormap: ")) - 1
    ret, frame = vc.read()
    times = []
    frame_idx = 0
    display = None
    # Populate images to calculate mean
    for i in range(20):
        ret, frame = vc.read()
        progbar.update(1)
    ## Main Loop
    while ret:
        ts = time.time()
        ret, frame = vc.read()
        original_frame = np.copy(frame)
        frame_idx += 1
        progbar.update(1)

        # Apply color map if selected
        if selection > 0 and frame is not None:
            colormapped = cv2.applyColorMap(frame, colormapDict[selection])
        else:
            colormapped = np.copy(frame)
    
        if args.video:
            display = colormapped
            video_out.write(display)
        if display is None:
            if args.track:
                display = np.hstack((original_frame, original_frame))
            else:
                display = frame
        # Show the resultant image
        cv2.imshow("Image", display)
        key = cv2.waitKey(20)
        if key == 27:
            break

        times.append(time.time() - ts)
    mean_processing_time = np.mean(times)
    print(f"Average time per frame: {mean_processing_time:.4f} seconds")
    if args.video:
        video_out.release()
        #resample_video_speed(args.video, mean_processing_time)
    
    vc.release()


if __name__ == "__main__":
    main()
